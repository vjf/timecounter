#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use lib 'lib';
use DateTime;

use Vjf::TimeCounter::Unit;

my $unit = Vjf::TimeCounter::Unit->new ({
        start_time  => DateTime->new(
            year    => 1982,
            month   => 11,
            day     => 15,
        ),
        stop_time   => DateTime->new(
            year    => 2012,
            month   => 8,
            day     => 1,
        ),
    });

print $unit;
