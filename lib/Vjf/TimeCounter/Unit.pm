use MooseX::Declare;

# we need dirty so that we can overload '""'
class Vjf::TimeCounter::Unit is dirty {
    use DateTime;
    use Carp qw(croak);
    has 'start_time' => (
        is          => 'ro',
        isa         => 'DateTime',
        required    => 1,
    ); 
    has 'stop_time' => (
        is          => 'ro',
        isa         => 'DateTime',
        required    => 1,
    );

    method BUILD($) {
        if ( $self->start_time >= $self->stop_time ) {
            croak "stop time must be later than start time";
        }
    };

    use overload '""' => sub {
        my $self = shift; 
        my $start_time = $self->start_time;
        my $stop_time  = $self->stop_time;
        "${$start_time->day}.$start_time->month().$start_time->year - ".
        "$stop_time->day.$stop_time->month.$stop_time->year";
    };
}
